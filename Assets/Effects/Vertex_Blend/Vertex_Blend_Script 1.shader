﻿Shader "Vertex_Blend_Script 1" {
	Properties {
		_Color ("Color", Color) = (1,1,1,1)

		_MainTex ("Albedo 1 (RGB)____________________________________MAT_01______________________________________", 2D) = "white" {}
		//_HeightMap ("Height 1 (RGB)", 2D) = "white" {}
		//_MainBumpMap ("Normal 1 (RGB)", 2D) = "bump" {}
		//_MRA1 ("Metallic 1(R) Glossiness 1(G) Occlusion 1(B)", 2D ) = "grey" {}

		_SecondaryTex ("Albedo 2 (RGB)____________________________________MAT_02______________________________________", 2D) = "white" {}
		//_SecondaryBumpMap ("Normal 2 (RGB)", 2D) = "bump" {}
		//_MRA2 ("Metallic 2(R) Glossiness 2(G) Occlusion 2(B)", 2D ) = "grey" {}
		
		_ThirdTex ("Albedo 3 (RGB)____________________________________MAT_03______________________________________", 2D) = "white" {}
		//_ThirdBumpMap ("Normal 3 (RGB)", 2D) = "bump" {}
		//_MRA3 ("Metallic 3(R) Glossiness 3(G) Occlusion 3(B)", 2D ) = "grey" {}

		_Hardness ("Blend Hardness ", Range(0.001,1)) = 0.5
		
		//arralax ("Parrallax Offset", Range(0,1)) = 0.5

	}
	SubShader {
		Tags { "RenderType"="Opaque" }
		LOD 200

		CGPROGRAM

		#pragma surface surf Standard fullforwardshadows

		#pragma target 3.0

		sampler2D _MainTex;
		sampler2D _SecondaryTex;
		sampler2D _ThirdTex;

		sampler2D _HeightMap;

		//sampler2D _MainBumpMap;
		//sampler2D _SecondaryBumpMap;
		//sampler2D _ThirdBumpMap;

		//sampler2D _MRA1;
		//sampler2D _MRA2;
		//sampler2D _MRA3;

		struct Input {
			float2 uv_MainTex;
			float2 uv_SecondaryTex;
			float2 uv_ThirdTex;
			float4 color:COLOR;
			//float3 viewDir;
		};

		//half _Metallic;
		fixed4 _Color;
		float _Hardness;
		//float _Parralax;

		void surf (Input IN, inout SurfaceOutputStandard o) {

			// Depliement de l'unique Height Map 
			fixed4 h = tex2D (_HeightMap, IN.uv_MainTex);
			
			// "Depliement" (???) du parralax suivant la MainTexture
			//float2 offset = ParallaxOffset(h,_Parralax,IN.viewDir);

			// Depliement des albedo suivant leurs planches Uv respectives
			fixed4 c1 = tex2D (_MainTex, IN.uv_MainTex /*+ offset*/) * _Color;
			fixed4 c2 = tex2D (_SecondaryTex, IN.uv_SecondaryTex ) * _Color;
			fixed4 c3 = tex2D (_ThirdTex, IN.uv_ThirdTex ) * _Color;
			
			// Depliement Normal Map suivant leurs planches Uv respectives
			//fixed3 n1 = UnpackNormal(tex2D (_MainBumpMap, IN.uv_MainTex + offset)) ; 
			//fixed3 n2 = UnpackNormal(tex2D (_SecondaryBumpMap, IN.uv_SecondaryTex )) ;
			//fixed3 n3 = UnpackNormal(tex2D (_ThirdBumpMap, IN.uv_ThirdTex)) ;

			float d = saturate((IN.color.r-h.r)/_Hardness); // Merge des 2 planche UV avec la Main
			float c = saturate((IN.color.g-h.g)/_Hardness); // Merge des 2 planche UV avec la Main

			// Depliement MRA suivant leurs planches Uv respectives
			//float4 mra1 = tex2D (_MRA1, IN.uv_MainTex + offset);
			//float4 mra2 = tex2D (_MRA2, IN.uv_SecondaryTex);
			//float4 mra3 = tex2D (_MRA3, IN.uv_ThirdTex);

			// Merge des Albedo dans l'output albedo
			o.Albedo = lerp(c1,c3,d).rgb;
			o.Albedo = lerp(o.Albedo,c2,c).rgb; 

		
			// Ici, je viens merge les differentes MRA dans un seul Output (pour chaque map), je doit le faire en deux fois car un "Lerp" ne peu CONTENIR que 2 elemnts SUIVANt un 3ème.
			//Metallic = lerp(mra1,mra2,d).r; // "Lerp" entre les 2 première MRA dans le channel "Red"
			//Smoothness = lerp(mra1,mra2,d).g; // "Lerp" entre les 2 première MRA dans le channel "Green"
			//Occlusion = lerp(mra1,mra2,d).b; // "Lerp" entre les 2 première MRA dans le channel "Blue"
			 
			//Metallic = lerp(o.Metallic,mra3,c).r; // "Lerp" entre le premier "Lerp" et la MRA3 dans la channel "Red" 
			//Smoothness = lerp(o.Smoothness,mra3,c).g; // "Lerp" entre le premier "Lerp" et la MRA3 dans la channel "Green"
			//Occlusion = lerp(o.Occlusion,mra3,c).b; // "Lerp" entre le premier "Lerp" et la MRA3 dans la channel "Blue"
 			
			// Ici, je viens merge les 3 Normal Map dans un seul Output
			//oat3 BlendNormal = lerp(n1,n2,d) ;
			//Normal = lerp(BlendNormal,n3,c) ;	


			
			
			
		}
		ENDCG
	}
	FallBack "Diffuse"
}
