using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class MoveCharacter : MonoBehaviour {
	public Camera Camera;
	//public Animator persoAnimator;
	public float moveSpeed; 
	public float stairHeight;
	public float characterHeight;
	//public ChaInteractSanctuaire sanctuaire;
	public Animation anim;


	/* Called before the first frame update */
	void Start() {
		/*
		* // Retrieve any GameObject with : 
		*
		* sanctuaire = GameObject.Find("Test").GetComponent<ChaInteractSanctuaire>();
		* sanctuaire = this.gameObject.transform.Find("Test").GetComponent<ChaInteractSanctuaire>();
		*/

		anim = GetComponent<Animation>();
		anim["Character.001|Idle"].wrapMode = WrapMode.Loop;
	}

	/* Called once per frame */
	public void Update() {
		/*
		if (Input.touchCount > 0) { // Tablet
			Touch touch = Input.GetTouch(0);
			if (touch.phase == TouchPhase.Ended) {
				FindPlaceToMove();
			}
		}
		*/

		if (Input.GetMouseButtonUp(0)) { // Desktop
			FindPlaceToMove();
		}
	}

	public void FindPlaceToMove() {
		int layerMask = 1 << 8; // Bit shift the index of the layer (8) to get a bit mask

		/* This would cast rays only against colliders in layer 8.
		 * But instead we want to collide against everything except layer 8. The ~ operator does this, it inverts a bitmask. */
		layerMask = ~layerMask;
	
		Ray ray = Camera.ScreenPointToRay(Input.mousePosition); // Computes the scene's 3D point from screen hit position
		RaycastHit hit;

		// Check if the ray intersects any objects excluding the player layer
		if (Physics.Raycast(ray, out hit, 1000)) { // layerMask ?
			if (hit.collider.name == "Ground") {
				StopAllCoroutines();
				StartCoroutine(Move(hit.point));
			}
		}
	}
    
	IEnumerator Move(Vector3 destination) {
		Vector3 source = this.gameObject.transform.position;
		destination.y += characterHeight; // distance between head and feet (the reference point of the character is in the head)

		Vector3 direction = destination - source;
		Vector3 angle = Vector3.RotateTowards(transform.forward, direction, 10, 0.0f);
		transform.rotation = Quaternion.LookRotation(angle);

		while (Mathf.Abs(direction.x) >= 1 || Mathf.Abs(direction.z) >= 1) {
			source = this.gameObject.transform.position;
			direction = destination - source;
			transform.Translate(Vector3.forward * moveSpeed);

			anim.Play("Character.001|Running");

			yield return new WaitForSeconds(.0001f);
		}

		anim.Play("Character.001|Idle");
	}

	void StopMove() {
		StopAllCoroutines();
		anim.Play("Character.001|Idle");
	}

	void OnCollisionEnter(Collision col) {
		switch (col.gameObject.name) {
			case "Ground":
				Debug.Log("Collision with Ground");
				break;

			case "":
				break;

			case "ArrowRight":
				Debug.Log("Collision with ArrowRight");
				break;

			default:
				//StopMove();
				break;
		}
	}

	void OnCollisionStay(Collision col) {
		if (col.gameObject.name == "RockCircleLeft1" || col.gameObject.name == "RockCircleRight1") {
			if (col.gameObject.transform.position.y < 1.65) {
				transform.Translate(0, stairHeight, 0);
			}
		}

		if (col.gameObject.name == "RockCircleLeft2" || col.gameObject.name == "RockCircleRight2") {
			if (col.gameObject.transform.position.y < 2.90) {
				transform.Translate(0, stairHeight, 0);
			}
		}

		if (col.gameObject.name == "RockCircleLeft3" || col.gameObject.name == "RockCircleRight3") {
			if (col.gameObject.transform.position.y < 4.15) {
				transform.Translate(0, stairHeight, 0);
			}
		}

		// switch (col.gameObject.name) {
		// 	default: break;
		// }
	}

	void OnTriggerEnter(Collider other) {
		Debug.Log("Character walked over " + other);
		StartCoroutine(LoadNextScene(other.gameObject.name));
	}

	IEnumerator LoadNextScene(string which) {
		// The Application loads the Scene in the background as the current Scene runs.
		// This is particularly good for creating loading screens.
		// You could also load the Scene by using sceneBuildIndex. In this case Scene2 has
		// a sceneBuildIndex of 1 as shown in Build Settings.

		Scene scene = SceneManager.GetActiveScene();
		string previousScene = "";
		string nextScene = "";
		AsyncOperation asyncLoad = null;

		switch (scene.name) {
			case "01_Sanctuary":
				nextScene = "02_Forest";
				break;

			case "02_Forest":
				previousScene = "01_Sanctuary";
				nextScene = "03_Mine";
				break;

			case "03_Mine":
				previousScene = "02_Forest";
				nextScene = "04_Castle";
				break;

			case "04_Castle":
				previousScene = "03_Mine";
				break;
		}

		if (which == "ArrowLeft" && scene.name != "01_Sanctuary") {
			asyncLoad = SceneManager.LoadSceneAsync(previousScene);
		} else if (which == "ArrowRight" && scene.name != "04_Castle") {
			asyncLoad = SceneManager.LoadSceneAsync(nextScene);
		}
		
		//asyncLoad.allowSceneActivation = false;

		// Wait until the asynchronous scene fully loads
		if (asyncLoad != null) {
			while (!asyncLoad.isDone) {
				float progress = Mathf.Clamp01(asyncLoad.progress / 0.9f);
				Debug.Log("Loading progress: " + (progress * 100) + "%");
				yield return null;
			}
		}
	}
}
