﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SageCharacter : MonoBehaviour
{

    public GameObject markSage;
    // Start is called before the first frame update
    void Start() {
        markSage.SetActive(false);
    }

    // Update is called once per frame
    void Update() {
        
    }

    void OnTriggerEnter(Collider other) {
        Debug.Log("Character Mage collide with " + other);
       	if(other.name=="Warrior") {
        	markSage.SetActive(true);
       	}
        
    }

    void OnTriggerExit (Collider other) {
        Debug.Log("Character Mage exit" + other);
       	if(other.name=="Warrior") {
        	markSage.SetActive(false);
       	}
        
    }
}
